defmodule Advent2022Test do
  use ExUnit.Case
  doctest Advent2022.Day1
  doctest Advent2022.Day2
  doctest Advent2022.Day3
  doctest Advent2022.Day4
  doctest Advent2022.Day5
  doctest Advent2022.Day6
  doctest Advent2022.Day7
end
