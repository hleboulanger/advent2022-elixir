defmodule Advent2022.Day4 do
  def part1(input) do
    input
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&fully_contained?/1)
    |> Enum.count(&(&1 == true))
  end

  def part2(input) do
    input
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&overlap?/1)
    |> Enum.count(&(&1 == true))
  end

  @doc """
  iex> Advent2022.Day4.fully_contained?("2-4,6-8")
  false

  iex> Advent2022.Day4.fully_contained?("2-8,3-7")
  true
  """
  def fully_contained?(input) do
    [l, r] = pairs_range_set(input)

    MapSet.subset?(l, r) || MapSet.subset?(r, l)
  end

  @doc """
  iex> Advent2022.Day4.overlap?("2-4,6-8")
  false

  iex> Advent2022.Day4.overlap?("5-7,7-9")
  true
  """
  def overlap?(input) do
    [l, r] = pairs_range_set(input)

    MapSet.intersection(l, r)
    |> Enum.count() > 0
  end

  defp pairs_range_set(input) do
    [l, r] = String.split(input, ",")

    [range_set(l), range_set(r)]
  end

  defp range_set(input) do
    [s, e] = String.split(input, "-")

    String.to_integer(s)..String.to_integer(e)
    |> MapSet.new()
  end
end
