defmodule Advent2022.Day5 do
  @state %{
    "1" => ["N", "W", "F", "R", "Z", "S", "M", "D"],
    "2" => ["S", "G", "Q", "P", "W"],
    "3" => ["C", "J", "N", "F", "Q", "V", "R", "W"],
    "4" => ["L", "D", "G", "C", "P", "Z", "F"],
    "5" => ["S", "P", "T"],
    "6" => ["L", "R", "W", "F", "D", "H"],
    "7" => ["C", "D", "N", "Z"],
    "8" => ["Q", "J", "S", "V", "F", "R", "N", "W"],
    "9" => ["V", "W", "Z", "G", "S", "M", "R"]
  }

  def part1(input) do
    [_, input] = String.split(input, "\n\n")

    input
    |> String.trim()
    |> String.split("\n")
    |> Enum.reduce(@state, fn line, state ->
      update_p1(line, state)
    end)
    |> Enum.flat_map(fn {_, v} ->
      Enum.take(v, 1)
    end)
    |> Enum.join()
  end

  def update_p1(line, state) do
    order = parse_line(line)

    {to_move, state} =
      Map.get_and_update!(state, order["from"], fn current ->
        Enum.split(current, order["count"])
      end)

    {_, state} =
      Map.get_and_update!(state, order["to"], fn current ->
        {current, Enum.reverse(to_move) ++ current}
      end)

    state
  end

  def update_p2(line, state) do
    order = parse_line(line)

    {to_move, state} =
      Map.get_and_update!(state, order["from"], fn current ->
        Enum.split(current, order["count"])
      end)

    {_, state} =
      Map.get_and_update!(state, order["to"], fn current ->
        {current, to_move ++ current}
      end)

    state
  end

  @doc """
  iex> Advent2022.Day5.parse_line("move 1 from 3 to 9")
  %{"count" => 1, "from" => "3", "to" => "9"}
  """
  def parse_line(line) do
    res = Regex.named_captures(~r/move (?<count>\d+) from (?<from>\d+) to (?<to>\d+)/, line)

    %{
      "count" => String.to_integer(res["count"]),
      "from" => res["from"],
      "to" => res["to"]
    }
  end

  def part2(input) do
    [_, input] = String.split(input, "\n\n")

    input
    |> String.trim()
    |> String.split("\n")
    |> Enum.reduce(@state, fn line, state ->
      update_p2(line, state)
    end)
    |> Enum.flat_map(fn {_, v} ->
      Enum.take(v, 1)
    end)
    |> Enum.join()
  end
end
