defmodule Advent2022.Day2 do
  @lost 0
  @draw 3
  @win 6

  def part1(input) do
    input
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&score_part1/1)
    |> Enum.sum()
  end

  @doc """
  A / X => Rock     (1)
  B / Y => Paper    (2)
  C / Z => Scissors (3)

  iex> Advent2022.Day2.score_part1("A Y")
  8
  iex> Advent2022.Day2.score_part1("B X")
  1
  iex> Advent2022.Day2.score_part1("C Z")
  6
  """
  def score_part1(line) do
    case String.split(line, " ") do
      ["A", "X"] -> @draw + 1
      ["A", "Y"] -> @win + 2
      ["A", "Z"] -> @lost + 3
      ["B", "X"] -> @lost + 1
      ["B", "Y"] -> @draw + 2
      ["B", "Z"] -> @win + 3
      ["C", "X"] -> @win + 1
      ["C", "Y"] -> @lost + 2
      ["C", "Z"] -> @draw + 3
    end
  end

  def part2(input) do
    input
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&score_part2/1)
    |> Enum.sum()
  end

  @doc """
  A / X => Rock     (1)
  B / Y => Paper    (2)
  C / Z => Scissors (3)

  X => lose
  Y => draw
  Z => win

  iex> Advent2022.Day2.score_part2("A Y")
  4
  iex> Advent2022.Day2.score_part2("B X")
  1
  iex> Advent2022.Day2.score_part2("C Z")
  7
  """
  def score_part2(line) do
    case String.split(line, " ") do
      ["A", "X"] -> @lost + 3
      ["A", "Y"] -> @draw + 1
      ["A", "Z"] -> @win + 2
      ["B", "X"] -> @lost + 1
      ["B", "Y"] -> @draw + 2
      ["B", "Z"] -> @win + 3
      ["C", "X"] -> @lost + 2
      ["C", "Y"] -> @draw + 3
      ["C", "Z"] -> @win + 1
    end
  end
end
