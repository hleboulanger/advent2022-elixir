defmodule Advent2022.Day3 do
  def part1(input) do
    input
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&unique_item/1)
    |> Enum.map(&item_value/1)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> String.trim()
    |> String.split("\n")
    |> Enum.chunk_every(3, 3)
    |> Enum.map(&unique_item_part2/1)
    |> Enum.map(&item_value/1)
    |> Enum.sum()
  end

  @doc """
  iex> Advent2022.Day3.unique_item("vJrwpWtwJgWrhcsFMMfFFhFp")
  "p"
  """
  def unique_item(input) do
    {l, r} = String.split_at(input, trunc(String.length(input) / 2))

    l = MapSet.new(String.graphemes(l))
    r = MapSet.new(String.graphemes(r))

    MapSet.intersection(l, r) |> MapSet.to_list() |> Enum.at(0)
  end

  @doc """
  iex> Advent2022.Day3.unique_item_part2(["vJrwpWtwJgWrhcsFMMfFFhFp", "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", "PmmdzqPrVvPwwTWBwg"])
  "r"
  """
  def unique_item_part2([l1, l2, l3]) do
    l1 = MapSet.new(String.graphemes(l1))
    l2 = MapSet.new(String.graphemes(l2))
    l3 = MapSet.new(String.graphemes(l3))

    MapSet.intersection(l1, l2) |> MapSet.intersection(l3) |> MapSet.to_list() |> Enum.at(0)
  end

  def item_value(input) do
    map = Map.new(Enum.zip(?a..?z, 1..26) ++ Enum.zip(?A..?Z, 27..52))

    <<v::utf8>> = input
    Map.get(map, v)
  end
end
