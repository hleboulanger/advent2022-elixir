defmodule Advent2022.Day1 do
  def part1(input) do
    input
    |> String.trim()
    |> String.split("\n\n")
    |> Enum.map(&sum/1)
    |> Enum.max()
  end

  def part2(input) do
    input
    |> String.trim()
    |> String.split("\n\n")
    |> Enum.map(&sum/1)
    |> Enum.sort()
    |> Enum.reverse()
    |> Enum.take(3)
    |> Enum.sum()
  end

  def part2_heap(input) do
    input =
      input
      |> String.trim()
      |> String.split("\n\n")
      |> Enum.map(&sum/1)
      |> Enum.into(Heap.max())

    (input |> Heap.root()) +
      (input |> Heap.pop() |> Heap.root()) +
      (input |> Heap.pop() |> Heap.pop() |> Heap.root())
  end

  def sum(lines) do
    lines
    |> String.split("\n")
    |> Enum.map(&String.to_integer/1)
    |> Enum.sum()
  end
end
