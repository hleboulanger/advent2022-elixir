defmodule Advent2022.Day6 do
  def part1(input) do
    solve(input, 4)
  end

  def part2(input) do
    solve(input, 14)
  end

  @doc """
  iex> Advent2022.Day6.solve("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 4)
  7

  iex> Advent2022.Day6.solve("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 14)
  19
  """
  def solve(input, window) do
    (input
     |> String.graphemes()
     |> Enum.chunk_every(window, 1)
     |> Enum.map(&count_unique/1)
     |> Enum.take_while(&(&1 < window))
     |> Enum.count()) + window
  end

  def count_unique(window) do
    MapSet.new(window)
    |> MapSet.size()
  end
end
