defmodule Mix.Tasks.Solve do
  @moduledoc "Solve AoC problem"
  @shortdoc "Solve AoC problem"

  use Mix.Task

  @impl Mix.Task
  def run(args) do
    day = args

    Mix.shell().info("Day: #{day}")
    input = File.read!("inputs/day#{day}.txt")

    res = apply(:"Elixir.Advent2022.Day#{day}", :part1, [input])
    Mix.shell().info("  - part 1: #{res}")
    res = apply(:"Elixir.Advent2022.Day#{day}", :part2, [input])
    Mix.shell().info("  - part 2: #{res}")
  end
end
