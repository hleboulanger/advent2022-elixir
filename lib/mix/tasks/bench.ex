defmodule Mix.Tasks.Bench do
  @moduledoc "Bench AoC solution"
  @shortdoc "Bench AoC solution"
  use Mix.Task

  @impl Mix.Task
  def run(day) do
    Mix.shell().info("Day: #{day}")
    input = File.read!("inputs/day#{day}.txt")

    Benchee.run(%{
      "day#{day}_part1" => fn -> apply(:"Elixir.Advent2022.Day#{day}", :part1, [input]) end,
      "day#{day}_part2" => fn -> apply(:"Elixir.Advent2022.Day#{day}", :part2, [input]) end
    })
  end
end
