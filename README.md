# Advent2022

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `advent2022` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:advent2022, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/advent2022>.


## Bench day1 naive

```
Day: 1
Operating System: Linux
CPU Information: Intel(R) Core(TM) i7-8650U CPU @ 1.90GHz
Number of Available Cores: 8
Available memory: 30.74 GB
Elixir 1.14.0
Erlang 25.1.2

Benchmark suite executing with the following configuration:
warmup: 2 s
time: 5 s
memory time: 0 ns
reduction time: 0 ns
parallel: 1
inputs: none specified
Estimated total run time: 14 s

Benchmarking day1_part1 ...
Benchmarking day1_part2 ...

Name                 ips        average  deviation         median         99th %
day1_part1        4.03 K      248.34 μs    ±24.46%      221.23 μs      449.40 μs
day1_part2        3.97 K      251.96 μs    ±25.18%      227.83 μs      504.29 μs

Comparison: 
day1_part1        4.03 K
day1_part2        3.97 K - 1.01x slower +3.61 μs
```

## Bench day1 binary heap
